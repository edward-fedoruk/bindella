import Head from 'next/head';
import React from 'react';
import PropTypes from 'prop-types';

const Layout = ({ children, title }) => (
  <div>
    <Head>
      <title>{title}</title>
      <meta charSet="utf-8" />
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    </Head>
    <div>
      {children}
    </div>
    <footer>I`m here to stay</footer>
  </div>
);

Layout.defaultProps = {
  title: 'Bindella',
};

Layout.propTypes = {
  children: PropTypes.any,
  title: PropTypes.string,
};

export default Layout;
